﻿using DynaReport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_App
{
    public class test:PageHeader
    {
        public test() {
            this.HeadingFont = "verdana";
            this.HeadingFontSize = 32D;
            this.HeadingText = "testTemplate";
            this.HeadingSizeWidth = 10D;
            this.TradingNameFont = "verdana";
            this.TradingNameFontSize = 32D;
            this.TradingNameText = "trading";
            this.TradingNameSizeWidth = 10D;

        }
    }
}
