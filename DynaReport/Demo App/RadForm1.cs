﻿using DynaReport;
using System;
using System.Data;
using System.Data.SqlClient;
using Telerik.Reporting;

namespace DemoApp
{
    public partial class RadForm1 : Telerik.WinControls.UI.RadForm
    {
        PageHeader ph;
        DetailBaseObject ds;
        PageFooter fs;
        DynaReportBuilder drb;
        DetailWithCrosstab dwct;




        public RadForm1()
        {
            InitializeComponent();
            ph = new PageHeader();
            ds = new DetailBaseObject();
            dwct = new DetailWithCrosstab();
            fs = new PageFooter();
            drb = new DynaReportBuilder();
            //drb.AddTable("Select companyName as Name FROM dbo.companies;", "Company");
            //drb.AddTable("Select * FROM dbo.companies;", "Companies");
            //drb.AddTable("SELECT productName as Name, productDescription as Description FROM dbo.products", "products");


            //page settings 
            dwct.Hieght = 5D;
            //Page_Width.Value=(decimal)drb.PageWidth;
            Page_Header_Height.Value = (decimal)ph.PageHeaderHieght;
            Detail_Hieght.Value = 0;
            Footer_Height.Value = 0;
            M_Top.Value = (decimal)drb.TopMargin;
            M_Bottom.Value = (decimal)drb.BottomMargin;
            M_Left.Value = (decimal)drb.LeftMargin;
            M_Right.Value = (decimal)drb.RightMargin;
            //heading settings
            H_Font.SelectedIndex = 0;
            H_FontSize.Value = (decimal)ph.HeadingFontSize;
            H_PosX.Value = (decimal)ph.HeadingLocationX;
            H_PosY.Value = (decimal)ph.HeadingLocationY;
            H_Width.Value = (decimal)ph.HeadingSizeWidth;
            H_Hieght.Value = (decimal)ph.HeadingSizeHieght;
            //trading name settings
            T_Font.SelectedIndex = 0; 
            T_FontSize.Value = (decimal)ph.TradingNameFontSize;
            T_PosX.Value = (decimal)ph.TradingNameLocationX;
            T_PosY.Value = (decimal)ph.TradingNameLocationY;
            T_WidthSpinner.Value = (decimal)ph.TradingNameSizeWidth;
            T_HieghtSpinner.Value = (decimal)ph.TradingNameSizeHieght;
        }

        private void Generate(object sender, EventArgs e)
        {
            var test = new Demo_App.test();
            var rs = new InstanceReportSource();
            rs.ReportDocument = drb.DynaBuild(
                pageHeader: test,
                detail: dwct, 
                pageFooter: fs,
                connectionString: ""
               );

            ph.HeadingFont = H_Font.SelectedItem.Text;
            ph.HeadingFontSize = (double)H_FontSize.Value;
            ph.TradingNameFont = T_Font.SelectedItem.Text;
            ph.HeadingText = H_Text.Text;
            ph.TradingNameText = T_Text.Text;
            ReportViewer1.ReportSource = rs;ReportViewer1.Refresh();
            ReportViewer1.RefreshReport();
        }
        
        private void H_FontSize_ValueChanged(object sender, EventArgs e)
        {
            ph.HeadingFontSize = (double)H_FontSize.Value;
        }

        private void TradingNamePosXChangeValue(object sender, EventArgs e)
        {
            ph.TradingNameLocationX=(double)T_PosX.Value;
        }

        private void TradingNamePosYChangeValue(object sender, EventArgs e)
        {
            ph.TradingNameLocationY = (double)T_PosY.Value;
        }

        private void T_WidthSpinner_ValueChanged(object sender, EventArgs e)
        {
           ph.TradingNameSizeWidth= (double)T_WidthSpinner.Value;
            
        }

        private void T_HieghtSpinner_ValueChanged(object sender, EventArgs e)
        {
           ph.TradingNameSizeHieght = (double)T_HieghtSpinner.Value;
        }

        private void T_FontSize_ValueChanged(object sender, EventArgs e)
        {
            ph.TradingNameFontSize = (double)T_FontSize.Value;
        }

        private void H_PosX_ValueChanged(object sender, EventArgs e)
        {
            ph.HeadingLocationX = (double)H_PosX.Value;
        }

        private void H_PosY_ValueChanged(object sender, EventArgs e)
        {
            ph.HeadingLocationY = (double)H_PosY.Value;
        }

        private void H_Width_ValueChanged(object sender, EventArgs e)
        {
            ph.HeadingSizeWidth = (double)H_Width.Value;
        }

        private void H_Hieght_ValueChanged(object sender, EventArgs e)
        {
            ph.HeadingSizeHieght = (double)H_Hieght.Value;
        }

        private void Page_Width_ValueChanged(object sender, EventArgs e)
        {
            drb.PageWidth = (double)Page_Width.Value;
        }

        private void Page_Header_Height_ValueChanged(object sender, EventArgs e)
        {
            ph.PageHeaderHieght = (double)Page_Header_Height.Value;
        }

        private void M_Right_ValueChanged(object sender, EventArgs e)
        {
            drb.RightMargin = (double)M_Right.Value;
        }

        private void M_Left_ValueChanged(object sender, EventArgs e)
        {
            drb.LeftMargin = (double)M_Left.Value;
        }

        private void M_Bottom_ValueChanged(object sender, EventArgs e)
        {
            drb.BottomMargin = (double)M_Bottom.Value;
        }

        private void M_Top_ValueChanged(object sender, EventArgs e)
        {
            drb.TopMargin = (double)M_Top.Value;
        }

        private void radCollapsiblePanel3_Expanded(object sender, EventArgs e)
        {

        }
    }
}
