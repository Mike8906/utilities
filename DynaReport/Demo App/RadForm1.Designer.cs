﻿namespace DemoApp
{
    partial class RadForm1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            this.H_Text = new Telerik.WinControls.UI.RadTextBoxControl();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.radCollapsiblePanel1 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.label11 = new System.Windows.Forms.Label();
            this.H_Hieght = new Telerik.WinControls.UI.RadSpinEditor();
            this.label12 = new System.Windows.Forms.Label();
            this.H_Width = new Telerik.WinControls.UI.RadSpinEditor();
            this.label13 = new System.Windows.Forms.Label();
            this.H_PosY = new Telerik.WinControls.UI.RadSpinEditor();
            this.label14 = new System.Windows.Forms.Label();
            this.H_PosX = new Telerik.WinControls.UI.RadSpinEditor();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.H_FontSize = new Telerik.WinControls.UI.RadSpinEditor();
            this.H_Font = new Telerik.WinControls.UI.RadDropDownList();
            this.radCollapsiblePanel2 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.label9 = new System.Windows.Forms.Label();
            this.T_HieghtSpinner = new Telerik.WinControls.UI.RadSpinEditor();
            this.label10 = new System.Windows.Forms.Label();
            this.T_WidthSpinner = new Telerik.WinControls.UI.RadSpinEditor();
            this.label8 = new System.Windows.Forms.Label();
            this.T_PosY = new Telerik.WinControls.UI.RadSpinEditor();
            this.label7 = new System.Windows.Forms.Label();
            this.T_PosX = new Telerik.WinControls.UI.RadSpinEditor();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.T_Text = new Telerik.WinControls.UI.RadTextBoxControl();
            this.T_FontSize = new Telerik.WinControls.UI.RadSpinEditor();
            this.T_Font = new Telerik.WinControls.UI.RadDropDownList();
            this.radCollapsiblePanel3 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.label21 = new System.Windows.Forms.Label();
            this.M_Right = new Telerik.WinControls.UI.RadSpinEditor();
            this.label22 = new System.Windows.Forms.Label();
            this.M_Left = new Telerik.WinControls.UI.RadSpinEditor();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.M_Bottom = new Telerik.WinControls.UI.RadSpinEditor();
            this.label18 = new System.Windows.Forms.Label();
            this.M_Top = new Telerik.WinControls.UI.RadSpinEditor();
            this.Footer_Height = new Telerik.WinControls.UI.RadSpinEditor();
            this.label15 = new System.Windows.Forms.Label();
            this.Detail_Hieght = new Telerik.WinControls.UI.RadSpinEditor();
            this.label = new System.Windows.Forms.Label();
            this.Page_Header_Height = new Telerik.WinControls.UI.RadSpinEditor();
            this.Page_Width = new Telerik.WinControls.UI.RadSpinEditor();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.ReportViewer1 = new Telerik.ReportViewer.WinForms.ReportViewer();
            this.report12 = new DemoApp.Report1();
            this.report11 = new DemoApp.Report1();
            ((System.ComponentModel.ISupportInitialize)(this.H_Text)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).BeginInit();
            this.radCollapsiblePanel1.PanelContainer.SuspendLayout();
            this.radCollapsiblePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.H_Hieght)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H_Width)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H_PosY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H_PosX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H_FontSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H_Font)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel2)).BeginInit();
            this.radCollapsiblePanel2.PanelContainer.SuspendLayout();
            this.radCollapsiblePanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.T_HieghtSpinner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T_WidthSpinner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T_PosY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T_PosX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T_Text)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T_FontSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T_Font)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel3)).BeginInit();
            this.radCollapsiblePanel3.PanelContainer.SuspendLayout();
            this.radCollapsiblePanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.M_Right)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.M_Left)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.M_Bottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.M_Top)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Footer_Height)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Detail_Hieght)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Page_Header_Height)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Page_Width)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.report12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.report11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // H_Text
            // 
            this.H_Text.Location = new System.Drawing.Point(4, 22);
            this.H_Text.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.H_Text.Name = "H_Text";
            this.H_Text.Size = new System.Drawing.Size(117, 20);
            this.H_Text.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.label1.Location = new System.Drawing.Point(2, 2);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Report name";
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.button1.Location = new System.Drawing.Point(0, 578);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(201, 55);
            this.button1.TabIndex = 3;
            this.button1.Text = "Generate";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Generate);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radScrollablePanel1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(201, 578);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings";
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radScrollablePanel1.Location = new System.Drawing.Point(4, 19);
            this.radScrollablePanel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.radCollapsiblePanel1);
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.radCollapsiblePanel2);
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.radCollapsiblePanel3);
            this.radScrollablePanel1.PanelContainer.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(191, 553);
            this.radScrollablePanel1.Size = new System.Drawing.Size(193, 555);
            this.radScrollablePanel1.TabIndex = 5;
            this.radScrollablePanel1.Text = "radScrollablePanel1";
            // 
            // radCollapsiblePanel1
            // 
            this.radCollapsiblePanel1.AnimationInterval = 0;
            this.radCollapsiblePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCollapsiblePanel1.HeaderText = "Report Name settings";
            this.radCollapsiblePanel1.IsExpanded = false;
            this.radCollapsiblePanel1.Location = new System.Drawing.Point(0, 36);
            this.radCollapsiblePanel1.Margin = new System.Windows.Forms.Padding(2, 8, 2, 8);
            this.radCollapsiblePanel1.Name = "radCollapsiblePanel1";
            this.radCollapsiblePanel1.OwnerBoundsCache = new System.Drawing.Rectangle(0, 36, 191, 389);
            this.radCollapsiblePanel1.Padding = new System.Windows.Forms.Padding(8, 8, 8, 8);
            // 
            // radCollapsiblePanel1.PanelContainer
            // 
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.label11);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.H_Hieght);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.label12);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.H_Width);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.label13);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.H_PosY);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.label14);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.H_PosX);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.label1);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.label4);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.H_Text);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.label3);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.H_FontSize);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.H_Font);
            this.radCollapsiblePanel1.PanelContainer.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radCollapsiblePanel1.PanelContainer.Size = new System.Drawing.Size(0, 0);
            this.radCollapsiblePanel1.Size = new System.Drawing.Size(191, 18);
            this.radCollapsiblePanel1.TabIndex = 11;
            this.radCollapsiblePanel1.Text = "radCollapsiblePanel1";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 248);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(42, 13);
            this.label11.TabIndex = 29;
            this.label11.Text = "Hieght";
            // 
            // H_Hieght
            // 
            this.H_Hieght.DecimalPlaces = 2;
            this.H_Hieght.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.H_Hieght.Location = new System.Drawing.Point(3, 266);
            this.H_Hieght.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.H_Hieght.Name = "H_Hieght";
            this.H_Hieght.Size = new System.Drawing.Size(116, 20);
            this.H_Hieght.TabIndex = 28;
            this.H_Hieght.TabStop = false;
            this.H_Hieght.ValueChanged += new System.EventHandler(this.H_Hieght_ValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 208);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "Width";
            // 
            // H_Width
            // 
            this.H_Width.DecimalPlaces = 2;
            this.H_Width.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.H_Width.Location = new System.Drawing.Point(3, 226);
            this.H_Width.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.H_Width.Name = "H_Width";
            this.H_Width.Size = new System.Drawing.Size(116, 20);
            this.H_Width.TabIndex = 26;
            this.H_Width.TabStop = false;
            this.H_Width.ValueChanged += new System.EventHandler(this.H_Width_ValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 168);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 13);
            this.label13.TabIndex = 25;
            this.label13.Text = "Posisition Y";
            // 
            // H_PosY
            // 
            this.H_PosY.DecimalPlaces = 2;
            this.H_PosY.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.H_PosY.Location = new System.Drawing.Point(3, 186);
            this.H_PosY.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.H_PosY.Name = "H_PosY";
            this.H_PosY.Size = new System.Drawing.Size(116, 20);
            this.H_PosY.TabIndex = 24;
            this.H_PosY.TabStop = false;
            this.H_PosY.ValueChanged += new System.EventHandler(this.H_PosY_ValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 128);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(66, 13);
            this.label14.TabIndex = 23;
            this.label14.Text = "Posisition X";
            // 
            // H_PosX
            // 
            this.H_PosX.DecimalPlaces = 2;
            this.H_PosX.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.H_PosX.Location = new System.Drawing.Point(3, 146);
            this.H_PosX.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.H_PosX.Name = "H_PosX";
            this.H_PosX.Size = new System.Drawing.Size(116, 20);
            this.H_PosX.TabIndex = 22;
            this.H_PosX.TabStop = false;
            this.H_PosX.ValueChanged += new System.EventHandler(this.H_PosX_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 45);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Font";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 84);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Font Size";
            // 
            // H_FontSize
            // 
            this.H_FontSize.DecimalPlaces = 2;
            this.H_FontSize.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.H_FontSize.Location = new System.Drawing.Point(3, 104);
            this.H_FontSize.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.H_FontSize.Name = "H_FontSize";
            this.H_FontSize.Size = new System.Drawing.Size(116, 20);
            this.H_FontSize.TabIndex = 7;
            this.H_FontSize.TabStop = false;
            this.H_FontSize.ValueChanged += new System.EventHandler(this.H_FontSize_ValueChanged);
            // 
            // H_Font
            // 
            radListDataItem1.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radListDataItem1.Text = "Arial";
            radListDataItem2.Font = new System.Drawing.Font("Harrington", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radListDataItem2.Text = "Harrington";
            this.H_Font.Items.Add(radListDataItem1);
            this.H_Font.Items.Add(radListDataItem2);
            this.H_Font.Location = new System.Drawing.Point(3, 63);
            this.H_Font.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.H_Font.Name = "H_Font";
            this.H_Font.Size = new System.Drawing.Size(117, 20);
            this.H_Font.TabIndex = 6;
            // 
            // radCollapsiblePanel2
            // 
            this.radCollapsiblePanel2.AnimationInterval = 0;
            this.radCollapsiblePanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCollapsiblePanel2.HeaderText = "Trading Name Settings";
            this.radCollapsiblePanel2.IsExpanded = false;
            this.radCollapsiblePanel2.Location = new System.Drawing.Point(0, 18);
            this.radCollapsiblePanel2.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.radCollapsiblePanel2.Name = "radCollapsiblePanel2";
            this.radCollapsiblePanel2.OwnerBoundsCache = new System.Drawing.Rectangle(0, 18, 191, 401);
            this.radCollapsiblePanel2.Padding = new System.Windows.Forms.Padding(8, 8, 8, 8);
            // 
            // radCollapsiblePanel2.PanelContainer
            // 
            this.radCollapsiblePanel2.PanelContainer.Controls.Add(this.label9);
            this.radCollapsiblePanel2.PanelContainer.Controls.Add(this.T_HieghtSpinner);
            this.radCollapsiblePanel2.PanelContainer.Controls.Add(this.label10);
            this.radCollapsiblePanel2.PanelContainer.Controls.Add(this.T_WidthSpinner);
            this.radCollapsiblePanel2.PanelContainer.Controls.Add(this.label8);
            this.radCollapsiblePanel2.PanelContainer.Controls.Add(this.T_PosY);
            this.radCollapsiblePanel2.PanelContainer.Controls.Add(this.label7);
            this.radCollapsiblePanel2.PanelContainer.Controls.Add(this.T_PosX);
            this.radCollapsiblePanel2.PanelContainer.Controls.Add(this.label5);
            this.radCollapsiblePanel2.PanelContainer.Controls.Add(this.label2);
            this.radCollapsiblePanel2.PanelContainer.Controls.Add(this.label6);
            this.radCollapsiblePanel2.PanelContainer.Controls.Add(this.T_Text);
            this.radCollapsiblePanel2.PanelContainer.Controls.Add(this.T_FontSize);
            this.radCollapsiblePanel2.PanelContainer.Controls.Add(this.T_Font);
            this.radCollapsiblePanel2.PanelContainer.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.radCollapsiblePanel2.PanelContainer.Size = new System.Drawing.Size(0, 0);
            this.radCollapsiblePanel2.Size = new System.Drawing.Size(191, 18);
            this.radCollapsiblePanel2.TabIndex = 12;
            this.radCollapsiblePanel2.Text = "radCollapsiblePanel2";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 256);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Hieght";
            // 
            // T_HieghtSpinner
            // 
            this.T_HieghtSpinner.DecimalPlaces = 2;
            this.T_HieghtSpinner.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.T_HieghtSpinner.Location = new System.Drawing.Point(14, 274);
            this.T_HieghtSpinner.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.T_HieghtSpinner.Name = "T_HieghtSpinner";
            this.T_HieghtSpinner.Size = new System.Drawing.Size(116, 20);
            this.T_HieghtSpinner.TabIndex = 20;
            this.T_HieghtSpinner.TabStop = false;
            this.T_HieghtSpinner.ValueChanged += new System.EventHandler(this.T_HieghtSpinner_ValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 216);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Width";
            // 
            // T_WidthSpinner
            // 
            this.T_WidthSpinner.DecimalPlaces = 2;
            this.T_WidthSpinner.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.T_WidthSpinner.Location = new System.Drawing.Point(14, 234);
            this.T_WidthSpinner.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.T_WidthSpinner.Name = "T_WidthSpinner";
            this.T_WidthSpinner.Size = new System.Drawing.Size(116, 20);
            this.T_WidthSpinner.TabIndex = 18;
            this.T_WidthSpinner.TabStop = false;
            this.T_WidthSpinner.ValueChanged += new System.EventHandler(this.T_WidthSpinner_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 176);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Posisition Y";
            // 
            // T_PosY
            // 
            this.T_PosY.DecimalPlaces = 2;
            this.T_PosY.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.T_PosY.Location = new System.Drawing.Point(15, 194);
            this.T_PosY.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.T_PosY.Name = "T_PosY";
            this.T_PosY.Size = new System.Drawing.Size(116, 20);
            this.T_PosY.TabIndex = 16;
            this.T_PosY.TabStop = false;
            this.T_PosY.ValueChanged += new System.EventHandler(this.TradingNamePosYChangeValue);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 136);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Posisition X";
            // 
            // T_PosX
            // 
            this.T_PosX.DecimalPlaces = 2;
            this.T_PosX.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.T_PosX.Location = new System.Drawing.Point(15, 154);
            this.T_PosX.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.T_PosX.Name = "T_PosX";
            this.T_PosX.Size = new System.Drawing.Size(116, 20);
            this.T_PosX.TabIndex = 14;
            this.T_PosX.TabStop = false;
            this.T_PosX.ValueChanged += new System.EventHandler(this.TradingNamePosXChangeValue);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 54);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Font";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 12);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Trading Name";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 94);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Font Size";
            // 
            // T_Text
            // 
            this.T_Text.Location = new System.Drawing.Point(12, 32);
            this.T_Text.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.T_Text.Name = "T_Text";
            this.T_Text.Size = new System.Drawing.Size(117, 20);
            this.T_Text.TabIndex = 4;
            // 
            // T_FontSize
            // 
            this.T_FontSize.DecimalPlaces = 2;
            this.T_FontSize.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.T_FontSize.Location = new System.Drawing.Point(12, 112);
            this.T_FontSize.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.T_FontSize.Name = "T_FontSize";
            this.T_FontSize.Size = new System.Drawing.Size(116, 20);
            this.T_FontSize.TabIndex = 11;
            this.T_FontSize.TabStop = false;
            this.T_FontSize.ValueChanged += new System.EventHandler(this.T_FontSize_ValueChanged);
            // 
            // T_Font
            // 
            radListDataItem3.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radListDataItem3.Text = "Arial";
            radListDataItem4.Font = new System.Drawing.Font("Harrington", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radListDataItem4.Text = "Harrington";
            this.T_Font.Items.Add(radListDataItem3);
            this.T_Font.Items.Add(radListDataItem4);
            this.T_Font.Location = new System.Drawing.Point(12, 72);
            this.T_Font.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.T_Font.Name = "T_Font";
            this.T_Font.Size = new System.Drawing.Size(117, 20);
            this.T_Font.TabIndex = 10;
            // 
            // radCollapsiblePanel3
            // 
            this.radCollapsiblePanel3.AnimationInterval = 0;
            this.radCollapsiblePanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCollapsiblePanel3.HeaderText = "PageSettings";
            this.radCollapsiblePanel3.IsExpanded = false;
            this.radCollapsiblePanel3.Location = new System.Drawing.Point(0, 0);
            this.radCollapsiblePanel3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radCollapsiblePanel3.Name = "radCollapsiblePanel3";
            this.radCollapsiblePanel3.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 191, 458);
            this.radCollapsiblePanel3.Padding = new System.Windows.Forms.Padding(8, 8, 8, 8);
            // 
            // radCollapsiblePanel3.PanelContainer
            // 
            this.radCollapsiblePanel3.PanelContainer.Controls.Add(this.label21);
            this.radCollapsiblePanel3.PanelContainer.Controls.Add(this.M_Right);
            this.radCollapsiblePanel3.PanelContainer.Controls.Add(this.label22);
            this.radCollapsiblePanel3.PanelContainer.Controls.Add(this.M_Left);
            this.radCollapsiblePanel3.PanelContainer.Controls.Add(this.label20);
            this.radCollapsiblePanel3.PanelContainer.Controls.Add(this.label19);
            this.radCollapsiblePanel3.PanelContainer.Controls.Add(this.label17);
            this.radCollapsiblePanel3.PanelContainer.Controls.Add(this.M_Bottom);
            this.radCollapsiblePanel3.PanelContainer.Controls.Add(this.label18);
            this.radCollapsiblePanel3.PanelContainer.Controls.Add(this.M_Top);
            this.radCollapsiblePanel3.PanelContainer.Controls.Add(this.Footer_Height);
            this.radCollapsiblePanel3.PanelContainer.Controls.Add(this.label15);
            this.radCollapsiblePanel3.PanelContainer.Controls.Add(this.Detail_Hieght);
            this.radCollapsiblePanel3.PanelContainer.Controls.Add(this.label);
            this.radCollapsiblePanel3.PanelContainer.Controls.Add(this.Page_Header_Height);
            this.radCollapsiblePanel3.PanelContainer.Controls.Add(this.Page_Width);
            this.radCollapsiblePanel3.PanelContainer.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radCollapsiblePanel3.PanelContainer.Size = new System.Drawing.Size(0, 0);
            this.radCollapsiblePanel3.Size = new System.Drawing.Size(191, 18);
            this.radCollapsiblePanel3.TabIndex = 13;
            this.radCollapsiblePanel3.Text = "radCollapsiblePanel3";
            this.radCollapsiblePanel3.Expanded += new System.EventHandler(this.radCollapsiblePanel3_Expanded);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(10, 296);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(75, 13);
            this.label21.TabIndex = 41;
            this.label21.Text = "Margin Right";
            // 
            // M_Right
            // 
            this.M_Right.DecimalPlaces = 2;
            this.M_Right.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.M_Right.Location = new System.Drawing.Point(10, 314);
            this.M_Right.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.M_Right.Name = "M_Right";
            this.M_Right.Size = new System.Drawing.Size(116, 20);
            this.M_Right.TabIndex = 40;
            this.M_Right.TabStop = false;
            this.M_Right.ValueChanged += new System.EventHandler(this.M_Right_ValueChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(10, 256);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(66, 13);
            this.label22.TabIndex = 39;
            this.label22.Text = "Margin Left";
            // 
            // M_Left
            // 
            this.M_Left.DecimalPlaces = 2;
            this.M_Left.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.M_Left.Location = new System.Drawing.Point(10, 274);
            this.M_Left.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.M_Left.Name = "M_Left";
            this.M_Left.Size = new System.Drawing.Size(116, 20);
            this.M_Left.TabIndex = 38;
            this.M_Left.TabStop = false;
            this.M_Left.ValueChanged += new System.EventHandler(this.M_Left_ValueChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(9, 15);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(67, 13);
            this.label20.TabIndex = 37;
            this.label20.Text = "Page Width";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(9, 136);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(79, 13);
            this.label19.TabIndex = 36;
            this.label19.Text = "Footer Hieght";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(10, 216);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(85, 13);
            this.label17.TabIndex = 35;
            this.label17.Text = "Margin Bottom";
            // 
            // M_Bottom
            // 
            this.M_Bottom.DecimalPlaces = 2;
            this.M_Bottom.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.M_Bottom.Location = new System.Drawing.Point(10, 234);
            this.M_Bottom.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.M_Bottom.Name = "M_Bottom";
            this.M_Bottom.Size = new System.Drawing.Size(116, 20);
            this.M_Bottom.TabIndex = 34;
            this.M_Bottom.TabStop = false;
            this.M_Bottom.ValueChanged += new System.EventHandler(this.M_Bottom_ValueChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(10, 176);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(65, 13);
            this.label18.TabIndex = 33;
            this.label18.Text = "Margin Top";
            // 
            // M_Top
            // 
            this.M_Top.DecimalPlaces = 2;
            this.M_Top.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.M_Top.Location = new System.Drawing.Point(10, 194);
            this.M_Top.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.M_Top.Name = "M_Top";
            this.M_Top.Size = new System.Drawing.Size(116, 20);
            this.M_Top.TabIndex = 32;
            this.M_Top.TabStop = false;
            this.M_Top.ValueChanged += new System.EventHandler(this.M_Top_ValueChanged);
            // 
            // Footer_Height
            // 
            this.Footer_Height.DecimalPlaces = 2;
            this.Footer_Height.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.Footer_Height.Location = new System.Drawing.Point(12, 154);
            this.Footer_Height.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Footer_Height.Name = "Footer_Height";
            this.Footer_Height.Size = new System.Drawing.Size(116, 20);
            this.Footer_Height.TabIndex = 31;
            this.Footer_Height.TabStop = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 97);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(75, 13);
            this.label15.TabIndex = 30;
            this.label15.Text = "Detail Hieght";
            // 
            // Detail_Hieght
            // 
            this.Detail_Hieght.DecimalPlaces = 2;
            this.Detail_Hieght.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.Detail_Hieght.Location = new System.Drawing.Point(12, 115);
            this.Detail_Hieght.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Detail_Hieght.Name = "Detail_Hieght";
            this.Detail_Hieght.Size = new System.Drawing.Size(116, 20);
            this.Detail_Hieght.TabIndex = 29;
            this.Detail_Hieght.TabStop = false;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(9, 54);
            this.label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(110, 13);
            this.label.TabIndex = 28;
            this.label.Text = "Page Header Hieght";
            // 
            // Page_Header_Height
            // 
            this.Page_Header_Height.DecimalPlaces = 2;
            this.Page_Header_Height.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.Page_Header_Height.Location = new System.Drawing.Point(12, 75);
            this.Page_Header_Height.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Page_Header_Height.Name = "Page_Header_Height";
            this.Page_Header_Height.Size = new System.Drawing.Size(116, 20);
            this.Page_Header_Height.TabIndex = 27;
            this.Page_Header_Height.TabStop = false;
            this.Page_Header_Height.ValueChanged += new System.EventHandler(this.Page_Header_Height_ValueChanged);
            // 
            // Page_Width
            // 
            this.Page_Width.DecimalPlaces = 2;
            this.Page_Width.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.Page_Width.Location = new System.Drawing.Point(12, 32);
            this.Page_Width.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Page_Width.Name = "Page_Width";
            this.Page_Width.Size = new System.Drawing.Size(116, 20);
            this.Page_Width.TabIndex = 26;
            this.Page_Width.TabStop = false;
            this.Page_Width.ValueChanged += new System.EventHandler(this.Page_Width_ValueChanged);
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.groupBox1);
            this.radPanel1.Controls.Add(this.button1);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.radPanel1.Location = new System.Drawing.Point(4, 4);
            this.radPanel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(201, 633);
            this.radPanel1.TabIndex = 6;
            // 
            // ReportViewer1
            // 
            this.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ReportViewer1.DocumentMapVisible = false;
            this.ReportViewer1.Location = new System.Drawing.Point(205, 4);
            this.ReportViewer1.Name = "ReportViewer1";
            this.ReportViewer1.ParametersAreaVisible = false;
            this.ReportViewer1.ShowDocumentMapButton = false;
            this.ReportViewer1.ShowHistoryButtons = false;
            this.ReportViewer1.ShowParametersButton = false;
            this.ReportViewer1.ShowPrintButton = false;
            this.ReportViewer1.ShowStopButton = false;
            this.ReportViewer1.Size = new System.Drawing.Size(480, 633);
            this.ReportViewer1.TabIndex = 0;
            // 
            // report12
            // 
            this.report12.Name = "Report1";
            // 
            // report11
            // 
            this.report11.Name = "Report1";
            // 
            // RadForm1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 641);
            this.Controls.Add(this.ReportViewer1);
            this.Controls.Add(this.radPanel1);
            this.Name = "RadForm1";
            this.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RadForm1";
            ((System.ComponentModel.ISupportInitialize)(this.H_Text)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            this.radCollapsiblePanel1.PanelContainer.ResumeLayout(false);
            this.radCollapsiblePanel1.PanelContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).EndInit();
            this.radCollapsiblePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.H_Hieght)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H_Width)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H_PosY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H_PosX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H_FontSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H_Font)).EndInit();
            this.radCollapsiblePanel2.PanelContainer.ResumeLayout(false);
            this.radCollapsiblePanel2.PanelContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel2)).EndInit();
            this.radCollapsiblePanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.T_HieghtSpinner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T_WidthSpinner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T_PosY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T_PosX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T_Text)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T_FontSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T_Font)).EndInit();
            this.radCollapsiblePanel3.PanelContainer.ResumeLayout(false);
            this.radCollapsiblePanel3.PanelContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel3)).EndInit();
            this.radCollapsiblePanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.M_Right)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.M_Left)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.M_Bottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.M_Top)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Footer_Height)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Detail_Hieght)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Page_Header_Height)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Page_Width)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.report12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.report11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.ReportViewer.WinForms.ReportViewer ReportViewer1;
        private Telerik.WinControls.UI.RadTextBoxControl H_Text;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private Telerik.WinControls.UI.RadTextBoxControl T_Text;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private Telerik.WinControls.UI.RadSpinEditor H_FontSize;
        private Telerik.WinControls.UI.RadDropDownList H_Font;
        private Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel1;
        private Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private Telerik.WinControls.UI.RadSpinEditor T_FontSize;
        private Telerik.WinControls.UI.RadDropDownList T_Font;
        private System.Windows.Forms.Label label8;
        private Telerik.WinControls.UI.RadSpinEditor T_PosY;
        private System.Windows.Forms.Label label7;
        private Telerik.WinControls.UI.RadSpinEditor T_PosX;
        private System.Windows.Forms.Label label9;
        private Telerik.WinControls.UI.RadSpinEditor T_HieghtSpinner;
        private System.Windows.Forms.Label label10;
        private Telerik.WinControls.UI.RadSpinEditor T_WidthSpinner;
        private System.Windows.Forms.Label label11;
        private Telerik.WinControls.UI.RadSpinEditor H_Hieght;
        private System.Windows.Forms.Label label12;
        private Telerik.WinControls.UI.RadSpinEditor H_Width;
        private System.Windows.Forms.Label label13;
        private Telerik.WinControls.UI.RadSpinEditor H_PosY;
        private System.Windows.Forms.Label label14;
        private Telerik.WinControls.UI.RadSpinEditor H_PosX;
        private Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel3;
        private System.Windows.Forms.Label label21;
        private Telerik.WinControls.UI.RadSpinEditor M_Right;
        private System.Windows.Forms.Label label22;
        private Telerik.WinControls.UI.RadSpinEditor M_Left;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private Telerik.WinControls.UI.RadSpinEditor M_Bottom;
        private System.Windows.Forms.Label label18;
        private Telerik.WinControls.UI.RadSpinEditor M_Top;
        private Telerik.WinControls.UI.RadSpinEditor Footer_Height;
        private System.Windows.Forms.Label label15;
        private Telerik.WinControls.UI.RadSpinEditor Detail_Hieght;
        private System.Windows.Forms.Label label;
        private Telerik.WinControls.UI.RadSpinEditor Page_Header_Height;
        private Telerik.WinControls.UI.RadSpinEditor Page_Width;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Report1 report11;
        private Report1 report12;
    }
}