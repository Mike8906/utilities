﻿using System.Drawing;
using Telerik.Reporting;

namespace DynaReport
{
    public class PageHeader
    {
        //protected variables
        protected PageHeaderSection phs;
        protected TextBox reportHeading;
        protected TextBox tradingName;
        protected PictureBox logo;
        protected double width= 3D;
        protected double hieght= 0.60000002384185791D;
        //page header height
        private double pageHeaderHieght=5;
        //setttings for the heading
        private string headingText;
        private string headingFont;
        private string headingTextboxName;
        private double headingLocationX;
        private double headingLocationY;
        private double headingSizeHieght;
        private double headingSizeWidth = 0;
        private double headingFontSize;

        //settings for the trading name
        private string tradingNameText;
        private string tradingNameFont;
        private string tradingTextboxName;
        private double tradingNameLocationX;
        private double tradingNameLocationY;
        private double tradingNameSizeHieght;
        private double tradingNameSizeWidth = 0;
        private double tradingNameFontSize;

        //settings for picturebox
        string logoMimeType = "image/jpeg";
        string logoName="logo";
        Color logoBackround=Color.Blue;
        double logoSizeHieght=2.1D;
        double logoSizeWidth= 2.1D;
        double logolocationX=0.2D;
        double logolocationY=0.2D;
        


        //internal methods for the api alone to consume
        internal PageHeaderSection PageHeaderBuilder()
        {
            logo = new PictureBox();
            logo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(LogolocationX), Telerik.Reporting.Drawing.Unit.Cm(LogolocationY));
            logo.MimeType = LogoMimeType;
            logo.Name = LogoName;
            logo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(LogoSizeWidth), Telerik.Reporting.Drawing.Unit.Cm(LogoSizeHieght));
            logo.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.Stretch;
            logo.Style.BackgroundColor = Color.Blue;
            phs = new PageHeaderSection();
            phs.Items.AddRange(
                new Telerik.Reporting.ReportItemBase[] {
                    CreateTextBox(
                        reportHeading,
                        HeadingText,
                        HeadingTextboxName,
                        HeadingLocationX,
                        HeadingLocationY,
                        HeadingSizeHieght,
                        HeadingSizeWidth,
                        HeadingFont,
                        HeadingFontSize),
                    CreateTextBox(
                        tradingName,
                        TradingNameText,
                        TradingTextboxName,
                        TradingNameLocationX,
                        TradingNameLocationY,
                        TradingNameSizeHieght,
                        TradingNameSizeWidth,
                        TradingNameFont,
                        TradingNameFontSize),
                    logo
                }
            );
            phs.Height= Telerik.Reporting.Drawing.Unit.Cm(PageHeaderHieght);
            phs.Name = "PageHeader";
            
            return phs;
        }

        internal TextBox CreateTextBox(TextBox source, string text, string name, double locationX, double locationY, double sizeHieght, double sizeWidth, string font,double fontsize)
        {
            source = new TextBox();
            source.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(locationX), Telerik.Reporting.Drawing.Unit.Cm(locationY));
            source.Name = name;
            source.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(sizeWidth), Telerik.Reporting.Drawing.Unit.Cm(sizeHieght));
            source.Value = text;
            source.Style.Font.Name = font;
            source.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(fontsize);
            return source;
        }




        public double TradingNameLocationX
        {
            get
            {
                if (tradingNameLocationX == 0) { return 2.6000001430511475D; }
                return this.tradingNameLocationX;
            }
            set
            {
                this.tradingNameLocationX = value;
            }
        }

        public double TradingNameLocationY
        {
            get
            {
                if (tradingNameLocationY == 0) { return 0.3999999463558197D; }
                return this.tradingNameLocationY;
            }
            set
            {
                this.tradingNameLocationY = value;
            }
        }

        public double HeadingSizeHieght
        {
            get
            {
                if (headingSizeHieght == 0) { return hieght; }
                return this.headingSizeHieght;
            }
            set
            {
                this.headingSizeHieght = value;
            }
        }

        public double HeadingSizeWidth
        {
            get
            {
                if (headingSizeWidth == 0) { return width; }
                return this.headingSizeWidth;
            }
            set
            {
                this.headingSizeWidth = value;
            }
        }

        public double HeadingLocationX
        {
            get
            {
                if (headingLocationX == 0) { return 2.6000001430511475D; }
                return this.headingLocationX;
            }
            set
            {
                this.headingLocationX = value;
            }
        }

        public double HeadingLocationY
        {
            get
            {
                if (headingLocationY == 0) { return 1.5000001192092896D; }
                return this.headingLocationY;
            }
            set
            {
                this.headingLocationY = value;
            }
        }

        public double TradingNameSizeHieght
        {
            get
            {
                if (tradingNameSizeHieght == 0) { return hieght; }
                return this.tradingNameSizeHieght;
            }
            set
            {
                this.tradingNameSizeHieght = value;
            }
        }

        public double TradingNameSizeWidth
        {
            get
            {
                if (tradingNameSizeWidth == 0) { return width; }
                return this.tradingNameSizeWidth;
            }
            set
            {
                this.tradingNameSizeWidth = value;
            }
        }
    
        public string HeadingText
        {
            get
            {
                return this.headingText;
            }
            set
            {
                this.headingText = value;
            }
        }

        public string TradingNameText
        {
            get
            {
                return this.tradingNameText;
            }
            set
            {
                this.tradingNameText = value;
            }
        }

        public string HeadingFont
        {
            get
            {
                if (string.IsNullOrEmpty(headingFont)) { return "Arial"; }
                return this.headingFont;
            }
            set
            {
                this.headingFont = value;
            }
        }

        public string TradingNameFont
        {
            get
            {
                if (string.IsNullOrEmpty(tradingNameFont)) { return "Arial"; }
                return this.tradingNameFont;
            }
            set
            {
                this.tradingNameFont = value;
            }
        }

        public string TradingTextboxName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(tradingTextboxName)) { return "TradingNameTextBox"; }
                return this.tradingTextboxName;
            }
            set
            {
                this.tradingTextboxName = value;
            }
        }

        public string HeadingTextboxName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(headingTextboxName)) { return "HeadingTextBox"; }

                return this.headingTextboxName;
            }
            set
            {
                this.headingTextboxName = value;
            }
        }

        public double LogoSizeHieght
        {
            get
            {
                return this.logoSizeHieght;
            }
            set
            {
                this.logoSizeHieght = value;
            }
        }

        public double LogoSizeWidth
        {
            get
            {
                return this.logoSizeWidth;
            }
            set
            {
                this.logoSizeWidth = value;
            }
        }

        public double LogolocationX
        {
            get
            {
                return this.logolocationX;
            }
            set
            {
                this.logolocationX = value;
            }
        }

        public double LogolocationY
        {
            get
            {
                return this.logolocationY;
            }
            set
            {
                this.logolocationY = value;
            }
        }

        public Color LogoBackround
        {
            get
            {
                return this.logoBackround;
            }
            set
            {
                this.logoBackround = value;
            }
        }

        public string LogoMimeType
        {
            get
            {
                return this.logoMimeType;
            }
            set
            {
                this.logoMimeType = value;
            }
        }

        public string LogoName
        {
            get
            {
                return this.logoName;
            }
            set
            {
                this.logoName = value;
            }
        }

        public double TradingNameFontSize
        {
            get
            {
                if (this.tradingNameFontSize == 0) { tradingNameFontSize = 10D; }
                return this.tradingNameFontSize;
            }
            set
            {
                this.tradingNameFontSize = value;
            }
        }

        public double HeadingFontSize
        {
            get
            {
                if (this.headingFontSize == 0) { headingFontSize = 10D; }
                return this.headingFontSize;
            }
            set
            {
                this.headingFontSize = value;
            }
        }

        public double PageHeaderHieght
        {
            get
            {
                return this.pageHeaderHieght;
            }
            set
            {
                this.pageHeaderHieght = value;
            }
        }
    }
}
