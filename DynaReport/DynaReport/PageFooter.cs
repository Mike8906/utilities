﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;
using Telerik.Reporting.Drawing;

namespace DynaReport
{
    public class PageFooter
    {
        //protected variables
        protected PageFooterSection pfs;
        protected TextBox reportPrintedOn;
        protected TextBox pageNofX;
        protected PictureBox logo;
        protected double width = 8D;
        protected double hieght = 0.60000002384185791D;
        //page header height
        private double pageFooterHieght = 8D;
        //setttings for the printedOn
        private string printedOnText;
        private string printedOnFont;
        private string printedOnTextboxName;
        private double printedOnLocationX;
        private double printedOnLocationY;
        private double printedOnSizeHieght;
        private double printedOnSizeWidth = 0;
        private double printedOnFontSize;

        //settings for the trading name
        private string pageNofXText;
        private string pageNofXFont;
        private string tradingTextboxName;
        private double pageNofXLocationX;
        private double pageNofXLocationY;
        private double pageNofXSizeHieght;
        private double pageNofXSizeWidth = 0;
        private double pageNofXFontSize;

        



        //internal methods for the api alone to consume
        internal PageFooterSection PageFooterBuilder()
        {
            pfs = new PageFooterSection();
            pfs.Items.AddRange(
                new Telerik.Reporting.ReportItemBase[] {
                    CreateTextBox(
                        reportPrintedOn,
                        PrintedOnText,
                        PrintedOnTextboxName,
                        PrintedOnLocationX,
                        PrintedOnLocationY,
                        PrintedOnSizeHieght,
                        PrintedOnSizeWidth,
                        PrintedOnFont,
                        PrintedOnFontSize),
                    CreateTextBox(
                        pageNofX,
                        PageNofXText,
                        PageNofXTextboxName,
                        PageNofXLocationX,
                        PageNofXLocationY,
                        PageNofXSizeHieght,
                        PageNofXSizeWidth,
                        PageNofXFont,
                        PageNofXFontSize)
                }
            );
            pfs.Height =Unit.Cm(PageFooterHieght);

            pfs.Name = "PageFooter";
            return pfs;
        }

        internal TextBox CreateTextBox(TextBox source, string text, string name, double locationX, double locationY, double sizeHieght, double sizeWidth, string font, double fontsize)
        {
            source = new TextBox();
            source.Location = new PointU(Unit.Cm(value: locationX),Unit.Cm(value: locationY));
            source.Name = name;
            source.Size = new SizeU(Unit.Cm(sizeWidth),Unit.Cm(sizeHieght));
            source.Value = text;
            source.Style.Font.Name = font;
            source.Style.Font.Size =Unit.Point(value: 8D);
            source.Style.TextAlign = HorizontalAlign.Left;
            source.Style.BackgroundColor = Color.Red;
            return source;
        }




        public Double PageNofXLocationX
        {
            get
            {
                if (pageNofXLocationX == 0) { return  16D; }
                return this.pageNofXLocationX;
            }
            set
            {
                this.pageNofXLocationX = value;
            }
        }

        public Double PageNofXLocationY
        {
            get
            {
                if (pageNofXLocationY == 0) { return 0D; }
                return this.pageNofXLocationY;
            }
            set
            {
                this.pageNofXLocationY = value;
            }
        }

        public Double PrintedOnSizeHieght
        {
            get
            {
                if (printedOnSizeHieght == 0) { return hieght; }
                return this.printedOnSizeHieght;
            }
            set
            {
                this.printedOnSizeHieght = value;
            }
        }

        public Double PrintedOnSizeWidth
        {
            get
            {
                if (printedOnSizeWidth == 0) { return width; }
                return this.printedOnSizeWidth;
            }
            set
            {
                this.printedOnSizeWidth = value;
            }
        }

        public Double PrintedOnLocationX
        {
            get
            {
                if (printedOnLocationX == 0) { return 0D; }
                return this.printedOnLocationX;
            }
            set
            {
                this.printedOnLocationX = value;
            }
        }

        public Double PrintedOnLocationY
        {
            get
            {
                if (printedOnLocationY == 0) { return 0D; }
                return this.printedOnLocationY;
            }
            set
            {
                this.printedOnLocationY = value;
            }
        }

        public Double PageNofXSizeHieght
        {
            get
            {
                if (pageNofXSizeHieght == 0) { return hieght; }
                return this.pageNofXSizeHieght;
            }
            set
            {
                this.pageNofXSizeHieght = value;
            }
        }
        public Double PageNofXSizeWidth
        {
            get
            {
                if (pageNofXSizeWidth == 0) { return 3D; }
                return this.pageNofXSizeWidth;
            }
            set
            {
                this.pageNofXSizeWidth = value;
            }
        }

        public string PrintedOnText
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.printedOnText)) { return "= \"Printed on \" + Now()"; }
                return this.printedOnText;
            }
            set
            {
                this.printedOnText = value;
            }
        }

        public string PageNofXText
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.pageNofXText)) { return "Page {PageNumber} of {PageCount}"; }
                return this.pageNofXText;
            }
            set
            {
                this.pageNofXText = value;
            }
        }

        public string PrintedOnFont
        {
            get
            {
                if (string.IsNullOrEmpty(printedOnFont)) { return "Arial"; }
                return this.printedOnFont;
            }
            set
            {
                this.printedOnFont = value;
            }
        }

        public string PageNofXFont
        {
            get
            {
                if (string.IsNullOrEmpty(pageNofXFont)) { return "Arial"; }
                return this.pageNofXFont;
            }
            set
            {
                this.pageNofXFont = value;
            }
        }

        public string PageNofXTextboxName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(tradingTextboxName)) { return "PageNofXTextBox"; }
                return this.tradingTextboxName;
            }
            set
            {
                this.tradingTextboxName = value;
            }
        }

        public string PrintedOnTextboxName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(printedOnTextboxName)) { return "PrintedOnTextBox"; }

                return this.printedOnTextboxName;
            }
            set
            {
                this.printedOnTextboxName = value;
            }
        }


        public Double PageNofXFontSize
        {
            get
            {
                if (this.pageNofXFontSize == 0) { pageNofXFontSize = 8D; }
                return this.pageNofXFontSize;
            }
            set
            {
                this.pageNofXFontSize = value;
            }
        }

        public Double PrintedOnFontSize
        {
            get
            {
                if (this.printedOnFontSize == 0) { printedOnFontSize = 8D; }
                return this.printedOnFontSize;
            }
            set
            {
                this.printedOnFontSize = value;
            }
        }

        public Double PageFooterHieght
        {
            get
            {
                return this.pageFooterHieght;
            }
            set
            {
                this.pageFooterHieght = value;
            }
        }
    }
}
