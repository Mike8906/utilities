﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using Telerik.Reporting;
using Telerik.Reporting.Drawing;

namespace DynaReport
{
    public class DynaReportBuilder
    {
        protected ObjectDataSource ods;
        protected Report dynaRep;
        protected PageHeaderSection pageHeader;
        protected PageFooterSection fs;
        protected MarginsU margins;
        protected DetailSection ds;
        protected List<SubClasses.Table> tables = new List<SubClasses.Table>();
        private double 
            leftMargin  = 10D, 
            rightMargin = 10D, 
            topMargin   = 10D, 
            bottomMargin= 10D;
        private double pageWidth=27.5;
        private bool landscape = true;
        private PaperKind paperType=PaperKind.A4;

        public DynaReportBuilder()
        {
           
        }

        internal string SqlBuilder(List<SubClasses.Table> tables)
        {
            string retstring="";
            foreach (SubClasses.Table item in tables)
            {
                retstring += item.SqlSelectCommand;
            }
            return retstring;
        }



        internal DataSet GetAllData(string connectionString)
        {
            //connectionString =
                //;

            string selectCommandText = SqlBuilder(tables);

            var adapter = new SqlDataAdapter(selectCommandText, connectionString);
            var dataSet = new DataSet();

            // The data set will be filled with three tables: ProductCategory, ProductSubcategory 
            // and Product as the select command contains three SELECT statements.
            adapter.Fill(dataSet);

            // Giving meaningful names for the tables so that we can use them later.
            for (int i = 0; i < dataSet.Tables.Count; i++)
            {
                SubClasses.Table tblname = tables[i];
                dataSet.Tables[i].TableName = tblname.Name;
            }
            return dataSet;
        }



        public void AddTable(string sqlCommand,string tableName)
        {
            var tbl = new SubClasses.Table();
            tbl.Name = tableName;
            tbl.SqlSelectCommand = sqlCommand;
            tables.Add(tbl);
        }

        public void ClearTables(string tableName)
        {
            tables.Clear();
        }

        public Report DynaBuild(PageHeader pageHeader, DetailBaseObject detail, PageFooter pageFooter,string connectionString)
        {
            if (tables.Count>0)
            {
                ods = new ObjectDataSource();
                ods.DataSource = GetAllData(connectionString);
            }
            //not used but height is set only
            ds = detail.DetailBuilder();
            
            fs = pageFooter.PageFooterBuilder();
            //set the margins
            margins =new MarginsU(
                Unit.Mm(LeftMargin),
                Unit.Mm(RightMargin),
                Unit.Mm(TopMargin),
                Unit.Mm(BottomMargin));
            //not used yet
            

            //declare the new report and assign the controls to it
            dynaRep = new Report();
            dynaRep.DataSource = ods;
            dynaRep.PageSettings.Margins = margins;
            dynaRep.PageSettings.PaperKind = PaperType;
            dynaRep.PageSettings.Landscape = true;
            dynaRep.Width =Unit.Cm(value: PageWidth);
            this.pageHeader = pageHeader.PageHeaderBuilder();
            dynaRep.Items.AddRange(new ReportItemBase[] { this.pageHeader,ds,fs });
            dynaRep.PageSettings.ContinuousPaper = false;
            
            return dynaRep;
        }

        public double LeftMargin
        {
            get
            {
                return this.leftMargin;
            }
            set
            {
                this.leftMargin = value;
            }
        }

        public double RightMargin
        {
            get
            {
                return this.rightMargin;
            }
            set
            {
                this.rightMargin = value;
            }
        }

        public double TopMargin
        {
            get
            {
                return this.topMargin;
            }
            set
            {
                this.topMargin = value;
            }
        }

        public double BottomMargin
        {
            get
            {
                return this.bottomMargin;
            }
            set
            {
                this.bottomMargin = value;
            }
        }

        public double AllMargin
        {
            set
            {
                this.bottomMargin = value;
                this.topMargin = value;
                this.rightMargin = value;
                this.leftMargin = value;
            }
        }
        public double TopAndBottomMargin
        {
            set
            {
                this.bottomMargin = value;
                this.topMargin = value;
            }
        }
        public double LeftAndRightMargin
        {
            set
            {
                this.rightMargin = value;
                this.leftMargin = value;
            }
        }

        public double PageWidth
        {
            get
            {
                return this.pageWidth;
            }
            set
            {
                this.pageWidth = value;
            }
        }

        public bool Landscape
        {
            get
            {
                return this.landscape;
            }
            set
            {
                this.landscape = value;
            }
        }

        public PaperKind PaperType
        {
            get
            {
                return this.paperType;
            }
            set
            {
                this.paperType = value;
            }
        }
    }
}
