﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace DynaReport.SubClasses
{
    public class Table 
    {
        public string SqlSelectCommand{get;set;}
        public string Name{ get;set;}
        public Telerik.Reporting.Table GenTable()
        {
            Telerik.Reporting.Table table1 = new Telerik.Reporting.Table();
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            var textBox6 = new Telerik.Reporting.TextBox();
            var textBox1 = new Telerik.Reporting.TextBox();
            var textBox7 = new Telerik.Reporting.TextBox();
            var textBox2 = new Telerik.Reporting.TextBox();
            table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2D)));
            table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2D)));
            table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.99999994039535522D)));
            table1.Body.SetCellContent(0, 0, textBox7);
            table1.Body.SetCellContent(0, 1, textBox2);
            tableGroup1.Name = "tableGroup";
            tableGroup1.ReportItem = textBox6;
            tableGroup2.Name = "group";
            tableGroup2.ReportItem = textBox1;
            table1.ColumnGroups.Add(tableGroup1);
            table1.ColumnGroups.Add(tableGroup2);
            table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            textBox7,
            textBox2,
            textBox6,
            textBox1});
            table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.6000001430511475D), Telerik.Reporting.Drawing.Unit.Cm(1.7000001668930054D));
            table1.Name = "table1";
            tableGroup3.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup3.Name = "detailTableGroup";
            table1.RowGroups.Add(tableGroup3);
            table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(2.0027084350585938D));

            return table1;

        }
    }
}
