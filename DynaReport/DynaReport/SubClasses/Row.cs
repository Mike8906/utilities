﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynaReport.SubClasses
{ 
    class Row
    {
        protected List<Column> columnList = new List<Column>();
        public string Text { get; set; }
        public string Font { get; set; }
        public bool Bold { get; set; }
        public bool Italics { get; set; }
        public bool UnderLined { get; set; }
        public double FontSize { get; set; }
        public void AddColunm(string name)
        {
            columnList.Add(new Column() { Name = name });
        }
        public void ClearColunms(string name)
        {
            columnList.Clear();
        }
    }
}
