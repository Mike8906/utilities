﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;
using Telerik.Reporting.Drawing;

namespace DynaReport
{
    public class DetailBaseObject
    {
        protected DetailSection ds;
        
        internal DetailSection DetailBuilder()
        {
            ds = new DetailSection();
            ds.Height = Unit.Cm(Hieght);
            ds.Style.BackgroundColor = BackroundColor;
            ds.Items.AddRange(ReportItems);
            return ds;
        }

        public double Hieght { get; set; }
        public Color BackroundColor { get; set; }
        public ReportItemBase[] ReportItems { get; set;}
    }
}
