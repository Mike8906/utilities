﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;

namespace ReportSwitcher
{
    public class SwitchReport
    {
        protected Report rep;
        protected Hashtable ht;
        
        public void AddReport(string reportName,Report reportObject)
        {
            ht.Add(reportName, reportObject);   
        }
        public void RemoveReport(string reportName)
        {
            ht.Remove(reportName);
        }
        public void ClearReports()
        {
            ht.Clear();
        }
        public Report Switch(string reportName)
        {
            if (ht.Count == 0)
            {
                Debug.WriteLine(message: "NO REPORTS ADDED", category: "ERROR");
            }
            else
            {
                foreach (DictionaryEntry item in ht)
                {
                    if (item.Key.ToString() == reportName)
                    {
                        rep = (Report) item.Value;
                    }
                }
                if (rep == null)
                {
                    Debug.WriteLine(message: "REPORT NOT FOUND",category:"ERROR");
                    rep = new Report();
                }

            }
            return rep;
        }
    }

    
}
