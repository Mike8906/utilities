﻿using System;
using System.Collections;
using Telerik.Reporting;
using Telerik.ReportViewer.WinForms;

namespace ReportSwitcher
{
    class Switcher
    {
        protected Hashtable ht;
        protected Report rep;
        public void Add(string reportName,Report reportObject)
        {
            ht.Add(reportName, reportObject);
        }
        public void Remove(string reportName)
        {
            ht.Remove(reportName);
        }
        public void Clear()
        {
            ht.Clear();
        }
        public Report Switch(string reportName){
            if (ht.Count == 0)
            {
                throw new Exception("No Reports In list");
            }
            foreach (DictionaryEntry item in ht)
            {
                if (item.Key.ToString() == reportName)
                {
                    rep = (Report)item.Value;
                }   
                else
                {
                    throw new Exception("Report Not Found");
                }
            }
            return rep;
        }
        public void SwitchAndSetReportSource(string reportName,ReportViewer viewer)
        {
            var rs = new InstanceReportSource();
            rs.ReportDocument = Switch(reportName);
            viewer.ReportSource = rs;
        }
    }
}
